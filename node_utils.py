trans_SELECT_OPERATOR = {
    '==': '=',
    '!=': '!=',
    '<=': '<=',
    '>=': '>=',
    '>': '>',
    '<': '<',
}

trans_logical_operator = {
    '||': ' OR ',
    '&&': ' AND ',
}


def all_append_str(p):
    l = len(p)
    i = 1
    if isinstance(p[i], str):
        r = p[i]
    elif isinstance(p[i], dict):
        r = p[i]['value']
    else:
        r = p[i].get_value()

    if l == 2:
        return r
    else:
        for i in range(2, l):
            if isinstance(p[i], str):
                r += p[i]
            elif isinstance(p[i], dict):
                r += p[i]['value']
            else:
                r += p[i].get_value()
    return r


def binds2str(binds):
    r = ''
    for i in binds:
        r += (i + ',')
    return r[0:-1]


class Node:
    def __init__(self, value, child_nodes=None):
        self.value = value
        if child_nodes is not None:
            for i, it in enumerate(child_nodes):
                if isinstance(it, dict):
                    child_nodes[i] = Node(it['value'])
                elif isinstance(it, str):
                    child_nodes[i] = Node(it)
                elif isinstance(it, Node):
                    pass
                else:
                    raise RuntimeError('Syntax tree parsing exceptions')
        self.child_nodes = child_nodes
        self.out_binds_done = None

    def cal_binds(self, outer_excludes=None):
        if self.child_nodes is None:
            return []
        else:
            binds = []
            for cn in self.child_nodes:
                binds += cn.out_binds(outer_excludes)
            return list(set(binds))

    def out_binds(self, outer_excludes=None):
        if self.out_binds_done is None:
            self.out_binds_done = self.cal_binds(outer_excludes)
        return self.out_binds_done

    def get_value(self, outer_excludes=None):
        if self.child_nodes is None:
            return self.value
        else:
            v = ''
            for cn in self.child_nodes:
                v += cn.get_value(outer_excludes)
            return v

    def get_value_no_excludes(self):
        if self.child_nodes is None:
            return self.value
        else:
            v = ''
            for cn in self.child_nodes:
                v += cn.get_value([])
            return v
