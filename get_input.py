from ply import lex
from ply import yacc
from node_utils import trans_SELECT_OPERATOR, trans_logical_operator, all_append_str, Node

tokens = (
    'STRING',
    'INT',
    'SELECT_OPERATOR',  # == | != | >= | <=
    'AND_OR',  # && ||
    'TABLE',
    'TUPLE',
    'GET',
    'PUT',
    'EXIST',
    'ANY',
    'USING',
    'DELETE'
)

reserved = {
    'TABLE': 'TABLE',
    'TUPLE': 'TUPLE',
    'GET': 'GET',
    'PUT': 'PUT',
    'exist': 'EXIST',
    'any': 'ANY',
    'using': 'USING',
    'DELETE': 'DELETE'
}

literals = [',', ';', '~', '@', '<', '>', '[', ']', '(', ')', '{', '}', '"', '=']


def t_STRING(t):
    r'[a-zA-Z_][a-zA-Z0-9_]*'
    t.type = reserved.get(t.value, 'STRING')  # Check for reserved words
    return t


def t_INT(t):
    r'[+-]?[0-9]+'
    return t


def t_SELECT_OPERATOR(t):
    r'==|!=|>=|<='
    return t


def t_AND_OR(t):
    r'&&|\|\|'
    return t


t_ignore = ' '


def t_error(t):
    print('err ch,', t.value[0])
    t.lexer.skip(1)


def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)


# yacc
start = 'program'

using_datebase = None
tuple2table = {}
table_name_dict = {}


def binds2alias(binds):
    r = ''
    for s in binds:
        try:
            r += (table_name_dict[tuple2table[s]] + ' ' + s + ',')
        except:
            print('errr')
    return r[0:-1]


def binds2alias_s(binds):
    r = ''
    for s in binds:
        r += (table_name_dict[tuple2table[s]] + ',')
    return r[0:-1]


class AnyNode(Node):
    # """any : ANY '(' head ',' condition ',' expression ')'"""
    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)
        self.head_node = self.child_nodes[2]
        self.condition_node = self.child_nodes[4]
        self.expression_node = self.child_nodes[6]
        self.out_binds_done = None

    def cal_binds(self, outer_excludes=None):
        return list(set(self.head_node.out_binds([])
                        + self.condition_node.out_binds(outer_excludes)
                        + self.expression_node.out_binds(outer_excludes)))

    def get_value(self, outer_excludes=None):
        binds_out = self.out_binds(outer_excludes)
        ins = 'NOT EXISTS (' \
              'SELECT * FROM ' + binds2alias(binds_out) + \
              ' WHERE NOT( ' \
              'NOT(' + self.condition_node.get_value(outer_excludes + binds_out) + \
              ') OR (' \
              + self.expression_node.get_value(outer_excludes + binds_out) + ')))'
        return ins


class ExistNode(Node):
    # """exist : EXIST '(' head ',' expression ')'"""
    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)
        self.head_node = self.child_nodes[2]
        self.expression_node = self.child_nodes[4]
        self.out_binds_done = None

    def cal_binds(self, outer_excludes=None):
        return list(set(self.head_node.out_binds([]) + self.expression_node.out_binds(outer_excludes)))

    def get_value(self, outer_excludes=None):
        binds_out = self.out_binds(outer_excludes)
        ins = 'EXISTS(' \
              'SELECT *' \
              ' FROM ' + binds2alias(binds_out) + \
              ' WHERE ' + self.expression_node.get_value(outer_excludes + binds_out) + \
              ')'
        return ins


class ExpressionNode(Node):
    """
    expression : term
               | term terms
    """

    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)


class TermNode(Node):
    """
    term : exist
            | any
            | factor
            | table_bind
    """

    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)
        self.only_node = child_nodes[0]  # exist any factor table_bind

    def out_binds(self, outer_excludes=None):
        if isinstance(self.only_node, AnyNode):
            return []
        elif isinstance(self.only_node, ExistNode):
            return []
        else:
            if self.out_binds_done is None:
                self.out_binds_done = self.cal_binds(outer_excludes)
            return self.out_binds_done


class TermsNode(Node):
    """
    terms : terms logical_operator term
          | logical_operator term
    """

    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)


class LogicalOperatorNode(Node):
    def __init__(self, value):
        super().__init__(trans_logical_operator[value])


class FactorNode(Node):
    """
    factor : compare_select
              | '~' '(' expression ')'
              |  '(' expression ')'
    """

    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)
        if len(child_nodes) > 1:
            self.expression_node = child_nodes[-2]
            self.cs_node = None
            self.one_node = child_nodes[-2]
        else:
            self.expression_node = None
            self.cs_node = child_nodes[0]
            self.one_node = child_nodes[0]

    def get_value(self, outer_excludes=None):
        inner_value = self.one_node.get_value(outer_excludes)
        if self.expression_node is None:
            return inner_value
        elif len(self.child_nodes) == 4:
            return ' NOT(' + inner_value + ') '
        else:
            return ' (' + inner_value + ') '


class CsNode(Node):
    """
    compare_select : domain_select SELECT_OPERATOR domain_select
            | domain_select '<' domain_select
            | domain_select '>' domain_select
            | domain_select SELECT_OPERATOR const
            | domain_select '<' const
            | domain_select '>' const
    """

    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)

    def get_value(self, outer_excludes=None):
        return self.get_value_no_excludes()


class DsNode(Node):
    """domain_select : tuple_name '[' '"' STRING '"'  ']'"""

    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)
        self.tu_name = child_nodes[0].get_value()

    def cal_binds(self, outer_excludes=None):
        if self.tu_name in outer_excludes:
            return []
        else:
            return [self.tu_name]

    def get_value(self, outer_excludes=None):
        rt = self.child_nodes[3].get_value()
        return self.tu_name + '.' + rt


class HeadNode(Node):
    """
    head : table_bind
            | tuple_name
    """

    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)
        self.only_one_node = self.child_nodes[0]

    def cal_binds(self, outer_excludes=None):
        if isinstance(self.only_one_node, TbNode):
            return self.only_one_node.out_binds(outer_excludes)
        else:
            if self.only_one_node.get_value() in outer_excludes:
                return []
            else:
                return [self.only_one_node.get_value([])]

    def get_value(self, outer_excludes=None):
        return self.get_value_no_excludes()


class DNode(Node):
    """
    head : ds
            | ds more_ds
    """

    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)
        self.ds_node = self.child_nodes[0]
        if len(self.child_nodes) > 1:
            self.more_ds_node = self.child_nodes[1]
        else:
            self.more_ds_node = None

    def get_value(self, outer_excludes=None):
        return self.get_value_no_excludes()


class MoreDsNode(Node):
    """
    head : ',' ds
            | ',' ds more_ds
    """

    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)

    def get_value(self, outer_excludes=None):
        return self.get_value_no_excludes()


class TbNode(Node):
    """table_bind : tuple_name '@' table_name"""

    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)
        self.only_one_node = self.child_nodes[0]

    def cal_binds(self, outer_excludes=None):
        return [self.only_one_node.get_value(outer_excludes)]

    def get_value(self, outer_excludes=None):
        return ' True '


def p_empty(p):
    """
    empty :
    """
    p[0] = {'value': ''}


def p_program(p):
    """
    program : using_db mixed_statement
    """


def p_using_db(p):
    """
    using_db : USING db_name ';'
    """
    using_database = p[2]
    print('use ' + using_database['value'])


def p_db_name(p):
    'db_name : STRING'
    p[0] = {'value': p[1]}


def p_mixed_statement(p):
    """
    mixed_statement : statements
                    | empty
    """


def p_statements(p):
    """
    statements : statements statement
               | statement
    """


def p_statement(p):
    """
    statement : declaration ';'
              | instruction ';'
              | assignment ';'
    """


def p_declaration(p):
    """
    declaration : tuple_declaration
                | table_declaration
    """


def p_tuple_declaration(p):
    """
    tuple_declaration : TUPLE '<' table_name '>' tuple_name empty
                         | TUPLE '<' table_name '>' tuple_name tuple_names
                         | TUPLE tuple_name empty
                         | TUPLE tuple_name tuple_names
    """
    if '<' in p[2]:
        tu_name = p[5]['value']
        ta_name = p[3]['value']
        tuple2table[tu_name] = ta_name
        if len(p[6]['value']) > 0:
            for i in p[6]['value']:
                tuple2table[i] = ta_name
    else:
        tu_name = p[2]['value']
        ta_name = None
        tuple2table[tu_name] = ta_name
        if len(p[3]['value']) > 0:
            for i in p[3]['value']:
                tuple2table[i] = ta_name


def p_tuple_names(p):
    """
    tuple_names : tuple_names ',' tuple_name
                | ',' tuple_name
    """
    if p[1] == ',':
        p[0] = {'value': [p[2]['value']]}
    else:
        p[0] = {'value': p[1]['value'] + [p[3]['value']]}


def p_tuple_name(p):
    """tuple_name : STRING"""
    p[0] = {'value': p[1]}


def p_table_declaration(p):
    """
    table_declaration : TABLE  table_name '=' '"' STRING '"' empty
                         | TABLE  table_name '=' '"' STRING '"' table_names
    """
    table_name_dict[p[2]['value']] = p[5]


def p_table_names(p):
    """
    table_names : table_names ',' table_name '=' '"' STRING '"'
                | ',' table_name '=' '"' STRING '"'
    """
    if len(p) > 1:
        if p[1] == ',':
            table_name_dict[p[2]['value']] = p[5]
        else:
            table_name_dict[p[3]['value']] = p[6]


def p_table_name(p):
    """table_name : STRING"""
    p[0] = {'value': p[1]}


def p_expression(p):
    """
    expression : term
               | term terms
    """
    p[0] = ExpressionNode(p[1:len(p)])


def p_terms(p):
    """
    terms : terms logical_operator term
          | logical_operator term
    """
    p[0] = TermsNode(p[1:len(p)])


def p_logical_operator(p):
    """
    logical_operator : AND_OR
    """
    p[0] = LogicalOperatorNode(p[1])


def p_term(p):
    """
    term : exist
            | any
            | factor
            | table_bind
    """
    p[0] = TermNode(p[1:len(p)])


def p_exist(p):
    """exist : EXIST '(' head ',' expression ')'"""
    p[0] = ExistNode(p[1:len(p)])


def p_any(p):
    """any : ANY '(' head ',' condition ',' expression ')'"""
    p[0] = AnyNode(p[1:len(p)])


def p_head(p):
    """
    head : table_bind
            | tuple_name
    """
    p[0] = HeadNode(p[1:len(p)])


def p_table_bind(p):
    """table_bind : tuple_name '@' table_name"""
    tuple2table[p[1]['value']] = p[3]['value']
    p[0] = TbNode(p[1:len(p)])


def p_condition(p):
    """condition : expression"""
    p[0] = p[1]


def p_factor(p):
    """
    factor : compare_select
              | '~' '(' expression ')'
              |  '(' expression ')'
    """
    p[0] = FactorNode(p[1:len(p)])


def p_const(p):
    """
    const : '"' STRING '"'
             | INT
    """
    p[0] = Node(all_append_str(p))


def p_compare_select(p):
    """
    compare_select : domain_select SELECT_OPERATOR domain_select
            | domain_select '<' domain_select
            | domain_select '>' domain_select
            | domain_select SELECT_OPERATOR const
            | domain_select '<' const
            | domain_select '>' const
    """
    p[0] = CsNode([p[1], Node(trans_SELECT_OPERATOR[p[2]]), p[3]])


def p_domain_select(p):
    """domain_select : tuple_name '[' '"' STRING '"'  ']'"""
    p[0] = DsNode(p[1:len(p)])


def p_instruction(p):
    """
    instruction : get_instruction
                | put_instruction
                | delete_instruction
    """


def p_put_instruction(p):
    """
    put_instruction : PUT '(' assigns ')' '{' expression '}'
                    | PUT '(' assigns ')' empty
    """
    if len(p) > 6:
        binds = list(set(p[3].out_binds([]) + p[6].out_binds([])))
        from_ins = binds2alias_s(binds)
        ins = 'UPDATE ' + from_ins + \
              ' SET ' + p[3].get_value() + \
              ' WHERE ' + p[6].get_value(binds)
    else:
        field_value_s = p[3].get_field_value_s()
        binds = list((p[3].out_binds([])))
        from_ins = binds2alias_s(binds)
        s1 = '('
        s2 = '('
        for it in field_value_s:
            s1 += it[0] + ','
            s2 += it[1] + ','
        s1 = s1[0:-1] + ')'
        s2 = s2[0:-1] + ')'
        ins = 'INSERT INTO ' + from_ins + s1 + \
              ' VALUES ' + s2
    print(ins)


class Assigns(Node):
    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)

    def get_field_value_s(self):
        if isinstance(self.child_nodes[1], MoreAssign):
            return [self.child_nodes[0].get_field_value_()] + self.child_nodes[1].get_field_value_s()
        else:
            return [self.child_nodes[0].get_field_value_()]

    def get_value(self, outer_excludes=None):
        if self.child_nodes is None:
            return self.value
        else:
            v = ''
            for cn in self.child_nodes:
                if isinstance(cn, Assign) or isinstance(cn, MoreAssign):
                    v += (cn.get_value(outer_excludes) + ',')
            return v[0:-1]


def p_assigns(p):
    """
    assigns : assign empty
              | assign more_assigns
    """
    p[0] = Assigns(p[1: len(p)])


class Assign(Node):
    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)

    def get_field_value_(self):
        return self.child_nodes[3].get_value([]), self.child_nodes[7].get_value([])

    def cal_binds(self, outer_excludes=None):
        return [self.child_nodes[0].get_value([])]

    def get_value(self, outer_excludes=None):
        return self.child_nodes[0].get_value([]) + '.' + self.child_nodes[3].get_value([]) \
               + '=' + self.child_nodes[-1].get_value()


def p_assign(p):
    """
    assign : tuple_name '[' '"' STRING '"'  ']' '=' const
    """
    p[0] = Assign(p[1: len(p)])


class MoreAssign(Node):
    def __init__(self, child_nodes):
        super().__init__(None, child_nodes)

    def get_field_value_s(self):
        if len(self.child_nodes) == 2:
            return [self.child_nodes[1].get_field_value_()]
        else:
            return [self.child_nodes[1].get_field_value_()] + self.child_nodes[2].get_field_value_s()

    def get_value(self, outer_excludes=None):
        if self.child_nodes is None:
            return self.value
        else:
            v = ''
            for cn in self.child_nodes:
                if isinstance(cn, Assign) or isinstance(cn, MoreAssign):
                    v += (cn.get_value(outer_excludes) + ',')
            return v[0:-1]


def p_more_assigns(p):
    """
    more_assigns : ',' assign
                 | ',' assign more_assigns
    """
    p[0] = MoreAssign(p[1: len(p)])


def p_get_instruction(p):
    """
    get_instruction : GET '(' domain ')' '{' expression '}'
    """
    binds = list(set(p[3].out_binds([]) + p[6].out_binds([])))
    from_ins = binds2alias(binds)
    ins = 'SELECT ' + p[3].get_value() + \
          ' FROM ' + from_ins + \
          ' WHERE ' + p[6].get_value(binds)
    print(ins)
    p[0] = {}
    p[0]['value'] = ins


def p_domain(p):
    """domain : domain_select
              | domain_select more_domain_select
    """
    p[0] = DNode(p[1:len(p)])


def p_more_domain_select(p):
    """
    more_domain_select : ',' domain_select
                          | ',' domain_select more_domain_select
    """
    p[0] = MoreDsNode(p[1:len(p)])


def p_delete_instruction(p):
    """
    delete_instruction : DELETE '(' tuple_name ')' '{' expression '}'
    """
    binds = list(set(p[6].out_binds([p[3]['value']])))
    fromins = binds2alias(p[3]['value'])
    ins = 'DELETE FROM ' + fromins + \
          ' WHERE ' + p[6].get_value(binds)
    print(ins)


def p_assignment(p):
    """
    assignment : view_assignment
                | table_assignment
    """


def p_table_assignment(p):
    """
    table_assignment : table_name '=' '"' STRING '"'
    """
    table_name_dict[p[1]['value']] = p[4]


def p_view_assignment(p):
    """
    view_assignment : table_name '=' get_instruction
    """
    print('DROP VIEW IF EXISTS __' + p[1]['value'])
    create_ins = 'CREATE VIEW __' + p[1]['value'] + ' AS ' + p[3]['value']
    table_name_dict[p[1]['value']] = '__' + p[1]['value']
    print(create_ins)


if __name__ == '__main__':
    data = ''
    with open("input.txt", "r", encoding='utf-8') as f:
        lines = f.readlines()
        for line in lines:
            data += line
    lexer = lex.lex()
    # lexer.input(data)
    # while True:
    #     token = lexer.token()
    #     print(token)
    #     if not token:
    #         break

    parser = yacc.yacc(debug=True)
    parser.parse(data)
    # print(0)
