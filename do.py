import pymysql

import os

connection = pymysql.connect(
    host='10.192.163.23',  # 数据库地址
    port=3306,
    user='root',  # 数据库用户名
    password='15926681095',  # 数据库密码
    db='spj_data',  # 数据库名称
    # charset = 'utf8 -- UTF-8 Unicode'
)

def b_print(res):
    for i in res:
        print(i)
    if len(res) == 0:
        print('None')
    print("=" * 10)


def main(do_print=True):
    sql_file = 'sql.txt'
    ins = 'python get_input.py > ' + sql_file
    os.system(ins)
    cursor = connection.cursor()
    results = []
    with open(sql_file, 'r') as f:
        lines = f.readlines()

        for line in lines:
            sql = line[0:-1] + ';'
            # execute执行操作
            # print('-sql-: ', sql)
            cursor.execute(sql)
            result = cursor.fetchall()
            connection.commit()
            # print(type(result), cursor.rowcount)
            if do_print:
                b_print(result)
            results.append(result)

    # 关闭游标
    cursor.close()
    # 关闭连接
    connection.close()

    return results


def do_logicSQL(input: str) -> None:
    with open('input.txt', 'w') as f:
        f.write(input)
    main(do_print=True)


def get_logicSQL(input: str) -> list:
    with open('input.txt', 'w') as f:
        f.write(input)
    return main(do_print=False)


if __name__ == '__main__':
    # input_data = 'using spj_data;' \
    #              'TABLE S = "s", P = "p", J = "j", SPJ = "spj";' \
    #              'TUPLE < S > s, s1, s2;' \
    #              'TUPLE < P > p, p1;' \
    #              'TUPLE < SPJ > spj, spj1, spjx, spjy;' \
    #              'TUPLE < J > j, j1, jno;' \
    #              'TUPLE t, px;' \
    #              'GET(jno["JNO"]){' \
    #              'any(px@P, spjx["PNO"]==px["PNO"] && spjx["SNO"]=="S1", ' \
    #              'exist(spjy,spjy["JNO"]==jno["JNO"] && spjy["PNO"]==px["PNO"] && spjy["SNO"]==spjx["SNO"]))' \
    #              '};'
    #
    # results = get_logicSQL(input_data)
    # b_print(results[-1])
    main()